﻿using System.ComponentModel;

namespace TodoApp.DAL
{
    public enum EntryType
    {
        [Description("Ajout")]
        ADD,
        [Description("Correction")]
        FIXED,
        [Description("Mise à jour")]
        UPDATE,
        [Description("Terminé")]
        COMPLETED
    }
}
