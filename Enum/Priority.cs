﻿using System.ComponentModel;

namespace TodoApp.DAL
{
    public enum Priority
    {
        [Description("Bas")]
        LOW,
        [Description("Normal")]
        NORMAL,
        [Description("Haute")]
        HIGH
    }
}
