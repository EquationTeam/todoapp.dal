﻿using System;
using System.Linq;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;

namespace TodoApp.DAL
{
    /// <summary>
    /// Initialize manager for todo
    /// </summary>
    public class TodoManager
    {
        private readonly TodoContext context;

        public TodoManager()
        {
            context = new TodoContext();
            Todoes = context.Todoes;
            Timelines = context.Timelines;
        }

        /// <summary>
        /// Attach one timeline
        /// </summary>
        /// <param name="userId">User id</param>
        /// <param name="id">Todo id</param>
        /// <param name="timeline"></param>
        public virtual void Attach(int userId, int id, Timeline timeline)
        {
            var item = context.Todoes.Find(id);
            var user = context.UserProfiles.Find(userId);
            if (item == null || timeline == null || user == null)
            {
                throw new ArgumentNullException();
            }

            timeline.Date = DateTime.Now;
            timeline.User = user;

            item.Timeline.Add(timeline);

            context.Entry(item).State = EntityState.Modified;
            context.SaveChanges();
        }

        /// <summary>
        /// Remove all todo
        /// </summary>
        /// <param name="todo"></param>
        public virtual void Clear()
        {
            var todoList = context.Todoes.Include(r => r.Timeline);

            foreach (var todo in todoList)
            {
                clearTimeline(todo);
                context.Todoes.Remove(todo);
            }
            context.SaveChanges();
        }

        /// <summary>
        /// Set todo completed
        /// </summary>
        /// <param name="id">Todo id</param>
        public virtual void Completed(int id)
        {
            var item = context.Todoes.Find(id);
            if (item == null)
            {
                throw new ArgumentNullException();
            }

            item.Completed = true;
            item.Progress = 100;

            context.Entry(item).State = EntityState.Modified;
            context.SaveChanges();
        }

        /// <summary>
        /// Create new todo
        /// </summary>
        /// <param name="userId">User id</param>
        /// <param name="todo"></param>
        public virtual void Create(int userId, Todo todo)
        {
            var user = context.UserProfiles.Find(userId);
            if (user == null || todo == null)
            {
                throw new ArgumentNullException();
            }

            todo.Completed = false;
            todo.Create = DateTime.Now;
            todo.Progress = 0;
            todo.Text = todo.Text.Trim();
            todo.Timeline = new List<Timeline>();
            todo.User = user;

            context.Todoes.Add(todo);
            context.SaveChanges();
        }

        /// <summary>
        /// Edit todo
        /// Keep property created and completed
        /// </summary>
        /// <param name="todo"></param>
        public virtual void Edit(Todo todo)
        {
            var current = context.Todoes.Find(todo.Id);
            if (current == null)
            {
                throw new ArgumentNullException();
            }
            
            todo.Create = current.Create;
            todo.Completed = current.Progress == 100 ? true : false;

            context.Entry(todo).State = EntityState.Modified;
            context.SaveChanges();
        }

        /// <summary>
        /// Get todo by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual Todo FindTodoById(int id)
        {
            return ToList().SingleOrDefault(r => r.Id == id);
        }

        /// <summary>
        /// Get timeline by todo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual List<Timeline> FindTimelineByTodoId(int id)
        {
            return Timelines
                .Include(r => r.Todo)
                .Include(r => r.User)
                .Where(r => r.Todo.Id == id)
                .OrderByDescending(r => r.Date)
                .ToList();
        }

        /// <summary>
        /// Remove todo
        /// </summary>
        /// <param name="todo"></param>
        public virtual void Remove(Todo todo)
        {
            if (todo == null)
            {
                throw new ArgumentNullException();
            }

            clearTimeline(todo);

            context.Todoes.Remove(todo);
            context.SaveChanges();
        }

        /// <summary>
        /// List of todo with include
        /// </summary>
        /// <returns></returns>
        public virtual List<Todo> ToList()
        {
            return context.Todoes
                .Include(r => r.User)
                .Include(r => r.Timeline)
                .ToList();
        }

        #region Helper

        private void clearTimeline(Todo todo)
        {
            foreach (var timeline in todo.Timeline.ToList())
            {
                Timelines.Remove(timeline);
            }
        }

        #endregion

        public DbSet<Todo> Todoes { get; set; }
        public DbSet<Timeline> Timelines { get; set; }
    }
}
