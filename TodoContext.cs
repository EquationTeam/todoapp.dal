﻿using System.Data.Entity;

namespace TodoApp.DAL
{
    public class TodoContext : DbContext
    {
        public TodoContext() : base("name=TodoContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Todo> Todoes { get; set; }
        public DbSet<Timeline> Timelines { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
    }
}
