namespace TodoApp.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class FinalMigrations : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Todoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Create = c.DateTime(nullable: false),
                        Completed = c.Boolean(nullable: false),
                        Priority = c.Int(nullable: false),
                        Progress = c.Int(nullable: false),
                        Text = c.String(),
                        Title = c.String(),
                        User_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfile", t => t.User_UserId, cascadeDelete: true)
                .Index(t => t.User_UserId);
            
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Timelines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Text = c.String(),
                        Type = c.Int(nullable: false),
                        Todo_Id = c.Int(),
                        User_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Todoes", t => t.Todo_Id, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.User_UserId, cascadeDelete: true)
                .Index(t => t.Todo_Id)
                .Index(t => t.User_UserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Timelines", new[] { "User_UserId" });
            DropIndex("dbo.Timelines", new[] { "Todo_Id" });
            DropIndex("dbo.Todoes", new[] { "User_UserId" });
            DropForeignKey("dbo.Timelines", "User_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.Timelines", "Todo_Id", "dbo.Todoes");
            DropForeignKey("dbo.Todoes", "User_UserId", "dbo.UserProfile");
            DropTable("dbo.Timelines");
            DropTable("dbo.UserProfile");
            DropTable("dbo.Todoes");
        }
    }
}
