﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TodoApp.DAL
{
    public class Todo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime Create { get; set; }
        public bool Completed { get; set; }
        public Priority Priority { get; set; }
        public int Progress { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        public UserProfile User { get; set; }

        public virtual List<Timeline> Timeline { get; set; }
    }
}
