﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TodoApp.DAL
{
    public class Timeline
    {    
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Text { get; set; }
        public Todo Todo { get; set; }
        public EntryType Type { get; set; }
        public UserProfile User { get; set; }
    }
}
